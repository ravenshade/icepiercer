<!-- TITLE: Kael'thas Sunstrider -->
<!-- SUBTITLE: Crown Prince Kael'Thas Sunstrider of Quel'Thalas, the Sovereign of Spheres, Secondborn Heir to the Thalassian Throne, Descendent of the Great Phoenix, the Captain of the Legendary Icepiercer, Slayer of Earth Elemental Prince Ogrémoch  -->

# Kael'thas Sunstrider
 * Age 60
 * Height: 6' 5"
 * Voice: Smouldery
 * Race: High Elf
 * Class: Phoenix Sorcerer
 * Alignment: True Neutral

## Skills
* Perception
* Arcana
* Persuasion
* Deception
* History

## Languages
* Common
* Elvish
* Infernal
* Quel'dorei (Thalassian)
* Sylvan

## Tools
* Chess

# Achievements
## Accomplishments
* 
## Titles
* 


# Quests
* 

# Factions

## Relations

### Crown Prince of Quel'Thalas

# History

## Backstory
Now this is a story all about how Kael'Thas got flipped-turned upside down. 
And I'd like to take a moment, just sit right there.
I'll tell you how I became crown prince of quel'thalas, the heir

In the desert, pacific born and raised
On the palace grounds was where I spent most of my days
Flamin' out maxin' relaxin' ignitin' some fuel
Shootin' some f-balls right into the pool
When a couple of cultists were up to no good
Started making portals in my neighborhood
I got in one little firebolt and my sister got scared
She said "You're leavin' with Lilith to the straits of Kadir"

I whistled for Telenius and when he came near
No license on this smuggler, all black market gear
If anything I couldn't help but gasp and stare
But Lilith said nah, it's fine, let's just get past Kadir

I pulled up to Luskan about week seven or eight
And I yelled at a thiefling, "that's my money, you saytr"
Looked at this kingdom I was finally there
To sit on this boat as the prince of nowhere

# Inventory
* My sheet tbh