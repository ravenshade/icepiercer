<!-- TITLE: Leo -->
<!-- SUBTITLE: A quick summary of Leo -->


# Leo Alrich
A misunderstood traveler from another universe who has an accute fasination with a very strange plant. Everyone who hears his story automatically thinks he is insane, so he doesn't talk much now.


## Skills
* Animal Handeling
* Farming

## Languages
* Common
* Dwarvish
* Other-Common

## Tools
* Herbalism Kit
* Tinker's Tools
* Navigator's Tools

# Achievements
## Accomplishments
* Survived 2 consecutive blood contracts with The Lich
* Built a dock on an island

## Titles
* Sindorei
# Quests
*  Learn exactly where he is, and how he got here
*  Learn how to leave this "universe"
*  Get revenge 

# Factions

## Relations

* Member of a powerful, and broken family on a different world (probably in a different dimension)
# History
 * Born: ??/??/????
 * Height: 6' 2"
 * Race: Hooman

## Backstory

Captured and used as a test subject by a cazed wizard, Leo was sent through a portal that was intended to lead to another plane of existance. Instead, he was sent to an entirly different material plane. Leo wandered the new world trying to learn it's languages, customs, and social policies. So far he has only gotten one out of the three. His social interactions are awkward, his habits are unothodox to this world's people, and he has become very indifferent to the socaily accepted definition of right and wrong. He has come to base his moral system on gut feelings and what seems most natural.