<!-- TITLE: Tiefling Island (Infernus) -->
<!-- SUBTITLE: A burgeoning colony -->

# Mount Marsarabit and Associated Islands


### Qualities of Main Island

![Island](/uploads/island.png "Island")

Size: (140 sq miles.)
Longest contiguous strip of land: 20 miles
Sea life: mahi mahi, tuna, moonfish, swordfish, mullet, seaweed (nori)
Flora: poi, coconuts, taro, bananas, hog plum, Pokenoboy, mushrooms
Fauna: insects, bats, birds, turtles


### Demographics / Humanoid Resources

(3) Contractors

 * Farmer
 * Carpenter
 * Stonemason

(2) are suited to "high skill" work - 
* Devil's Advocate
* Astara

(9) are not really suited for work, but can do it in certain situations
* Des
* Mis
* Seven other children - 5 male, 2 female
		
(7) are over the age of 45, and substantially less vigorous than their counterparts.
* 5 female
* 2 male

(25) are in youth demographic

* 18 male
              1. 4 were in the Atticus faction, but defected
              2. 6 (including Quest) are still in the Atticus faction
              3. 8 were never in the Atticus faction
* 7 female
	 
(33) are in the "prime adult life" demographic (18-45)
 * 17 male
 * 16 female

#### Animals
* 20 chickens (chuck)
* 30 draft horses
* 8 warhorses

### Current Employment

#### Foraging
(9/9) children
(7/7) 45+ 
(5/33) adults
21 foragers

#### Farming
(15/33) adults
(15/25) youth - 8 males never in the Atticus Faction + 7 women
30 farmers - each farmer has a draft horse


#### Log refiners
(5/33) adults
(10/30) Ice Elementals
5 refiners + 10 Ice Elementals


#### Builders
(8/33) adults
(10/25) youths
(18/30) Ice Elementals
18 builders + Astara + 18 Ice Elementals


### Part time

#### Morning training
(21/25) youth - all except 4 women
(12/33) adults


#### Night classes - Thaumaturgy Training
Ten "trainers"
Rest "learners"
All

### Supplies

* Seeds - Pineapples, Wheat (decreased stock due to starting farms)
* 10 Fishing Supplies
* 5 Carpenter's Tools
* 40 chickens (chuck)
* 20 handaxes
* 20 spears
* 5 cauldrons
* 20 handaxes
* 1 cartographer's tools
* 50lb iron
* 34lb quality wood
* ropes
* blankets
* hammocks
* 35 clay pots / bowls
* Large amounts of "low quality" wood
* 8 cannons + 197 cannonballs
* Ship Supplies from "The Summer Sun" Ship - 4 warhorses, 3 dairy cows


### Interesting locations, Construction and Deforestation

> Initial Landing spot
* 20 "glass cones" to catch rainwater

> "Harbor Area"
* 3000 square feet of deforested area with ground suitable for building
* Beginning of stone docks; 100 feet of 1 foot thick retaining wall, capped off at sides. Area has had sand removed
* 1 "wooden deck" - potential base for a moveable structure?
* (3) storage buildings
* (5) accomodations buildings
* (1) Chicken coop - 20 chickens (chuck), cows
* (1) Communal firepit with crude chairs
* "Glyph of Warding" spell created by Norb's Divine Intervention
* arbitrarily high amount of "glass cones" to catch rainwater


> Sulfurus Hot Springs


> Mount Marsarabit

* DIscovered it was an Elemental Node of Fire, suitable for creation of Devestation Orbs of Fire. 

> Farmland

* Under the effects of plant growth
* Trenches dug, 30 horses, seeds planted