<!-- TITLE: Home -->
<!-- SUBTITLE: Wiki for the A team and B team to reference
 -->


# Wiki Organization
* [The Only Important Thing](/ships/icepiercer)
* [ships](/ships)
* nations
* organizations
* [characters](/characters)
* [places](/places)

# Recordings
* Operation Kill Cynthia Part 2: There Is No Operation Kill Cynthia
<iframe
   frameborder="0"
   width="75%"
   height="25%"
   src="https://drive.google.com/file/d/1HEmNtz5dRe6ZJOCVbNQlmczn92xl9kk8/preview?usp=sharing">
</iframe>

* Operation Kill Cynthia Part 1: A Night of Salt
<iframe
   frameborder="0"
   width="75%"
   height="25%"
   src="https://drive.google.com/file/d/12WQRIqeW8S2kD4r_uUzdiObA9aaxQZkE/preview?usp=sharing">
</iframe>

# Templates
* [PC](/templates/pc)
* [Ship](/templates/ship)