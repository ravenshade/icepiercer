<!-- TITLE: Ships -->
<!-- SUBTITLE: List of all Ships: controlled, friendly, hostile, neutral -->
# The A Team
## Controlled

[The Ice Piercer](/ships/icepiercer)
[The Sea Hawk](/ships/the-seahawk)
[The Kingfisher](/ships/kingfisher)

## Friendly

[The Swordfish](/ships/swordfish)

## Hostile

## Neutral

# The B Team
## Controlled

[The Flappy Flamingo](/flamingo)

## Friendly

## Hostile

## Neutral