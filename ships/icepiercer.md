``<!-- TITLE: Ice Piercer -->
<!-- SUBTITLE: Stats, crew, and navigation log for the A Team's ship -->

# Essential
## HP
Hull: 200/400 hp (30 threshold) AC 18, bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine (depending on the angle)
Helm: 75/75 hp AC 18 (speed of sails, 90 degree turn)
Spider Silk Sails (2): 150/150 hp AC 15 speed 60 (+/- 30 wind), -5 speed per 25dmg; 10 mph travel
Treebeard Balorbane: 238/238 hp AC 16, Ranged +10 hit 120/480ft, 5d10 + 6 (siege, double obj/structs)

## Ability
###### Dimension Door 1/1 (recharge dawn) - requires crew helm
- When you land, can displace water and air. Whoever is controlling ship.
###### Windwalk 1/1 (recharge in the eye of a storm) - requires crew helm
- Affects it and everything on the ship. Ship turns into wind.
- Treebeard is incapacitated during, all turns into wispy clouds.
###### Wards: Mordenkainen's Private Sanctum (effectively)
- No divination spells can target
- Sensors cannot go through barrier
# Stats

**The Ice Piercer**
Gargantuan vehicle (20 ft. by 70. feet)
Creature Capacity 30 crew, 20 passengers
Cargo Capacity 100 tons
Travel pace 10 miles per hour
STR 16
DEX 20
CON 17
INT 0
WIS 0
CHA 0

Damage Resistances: Fire (hull)
Damage Immunities poison, psychic, cold
Condition Immunities blinded, charmed, deafened, exhaustion, frightened, incapacitated, paralyzed, petrified, poisoned, prone, stunned, unconscious

### Aura
 * Damage Resistances: Fire
 * **Hallow**: celestials, elementals (exclude), fey, fiends, and undead cannot enter
 * **Hallow**: Cannot be charmed, feared, posessed by those beings either
 * **Hallow**: Damage Resistance: necrotic

### Hull
AC 15, Hitpoints 400, damage threshold 30 (20 w/o adamantite)

### Control: Helm
Armor Class 18
Hit Points 75
Move up to the speed of its sails, with one 90-degree turn. If the helm is destroyed, the ship can’t turn. Only controlled by those who used their blood.
* Blood: Norb, Leo, Atticus, Alte, Lilith, Hibiki, RBDash, Treebeard, HHPP
* Patron: Goddess  of Storms

### Movement: Spider Silk Sails
Armor Class 15
Hit Points 150; –5 ft. speed per 25 damage taken
Locomotion (water) sails, speed 60 ft.; 30 ft. while sailing into the wind; 90 ft. while sailing with the wind

### Weapon: Treebeard
Armor Class 16
Hit Points 238
Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 16 (3d6 + 6) bludgeoning damage.
Ranged Weapon Attack: +10 to hit, range 120/480 ft., one target. Hit: 33 (5d10 + 6) bludgeoning damage
Siege Monster. Treebeard deals double damage to objects and structures.

### Actions:
The ship can move using its helm. Treebeard may take an attack against a target in range. If a crew member is at the helm, they make take an action to cast _Dimension Door_, moving the ship and contents. This property recharges at dawn. They may also take an action to cast _Wind Walk_ on the ship, turning it into wispy clouds. Objects do not fall through the cloud in this state. Treebeard is incapacitated during this time. This property recharges when passing through the eye of a storm.

# Crew & Passengers

## Crew
### Info
The crew is composed of Ice Elementals, ritually bound to serve. They function as their respective statblock with the following changes:

* Their creature type is Construct
* They do not require air, food, drink, or sleep.
* If not on the boat for 24 consecutive hours, their max health begins to decrease by 1d12 for each hour off the boat. Their health may be reset by spending 8 consecutive hours on the boat.
* The crew can be remade in batches of 3 in an 8 hour long ritual, but there may only be 30 total crew members at a time.

15 guards, as per http://5etools.ravenshade.com/bestiary.html#guard_mm - They make their ability checks to run the ship with a +5 bonus

15 thugs, as per http://5etools.ravenshade.com/bestiary.html#thug_mm - instead of heavy crossbows, they launch shards of ice

### Members

Alive: 8/15 T, 4/15 G
(G) denotes guard, (T) thug
* (T) Dock Docker
* (T) Whale Smith
* (G) Salami - Chef
* (G) Forward - Rigger
* (G) Unto - Rigger
* (G) Dawn - Rigger
* (T) Toblin
* (T) Neo
* (T) Selena
* (T) Alleria - Sailor
* (T) Sylvanas - Sailor
* (T) Vereesa - Sailor
* (G) Tomtom - Navigator
* (G) Eleanor - Captain
* (G) Henry - First Mate
* (T) Mei
* (T) Percy
* (G) Belle - Librarian / Maid / Known Accordion player
* (G) Modrin - Waiter / Souschef / Known Suzelhorn player
* (G) Doc, Happy, Sneezy, Sleepy, Bashful, Grumpy, Dopey
* (G) Frohman
* (G) Pancakes
* (T) Lilith Jr.
* (T) Lilith Sr.
* (T) Kael'hunk Sunbather
* 2 lambs



## Former Passengers
Despair & Misery (aka Des & Mis) - Tiefling slaves, sisters, respectively 4 and 5 years old, purchased on March 30th.
Devil's Advocate - Tiefling slave. Old man, horns cut off, mouth heavily scarred, bad case of scoliosis. 
Fair Lady's (Former) Captain - a skeleton, stuck reading Henry Gallagher's Book of Rituals for 11 years.

# Cargo & Inventory
### Captain's Quarters (shipyard theme)
* 30,620p 51,216g 10,000s 1,200c
* Sextant, spyglass, compass
* Ship Deed of Quel'Thalas with Phoenix Signet (27)
* 100 parchment & elegant quill set
* (On Devil's Advocate) Wizard's Spellbook
* Wizard of Summersun Spellbook
* Clockwork Clock (Luskan)
* Clockwork Clock (Ipacas) 
* 300g Magic Ink
* Grandfather Clock Movement
* Deed - Master Engineer Alinor
* Blue sapphire (transparent blue-white) (1000g)
* 2 x Fire opal (translucent fiery red) (1000g)
* Opal (translucent pale blue with green and golden mottling) (1000g)
* 2 x Star ruby (translucent ruby with white star-shaped center) (1000g)
* 2 x Star sapphire (translucent blue sapphire with white star-shaped center) (1000g)
* Yellow sapphire (transparent fiery yellow or yellow-green) (1000g)
* 100 gp gemstones (×8):
* Amber (transparent watery gold to rich gold)
* Chrysoberyl (transparent yellow-green to pale green)
* Jade (translucent light green, deep green, or white)
* Jet (opaque deep black)
* Spinel (transparent red, red-brown, or deep green), ×4
* Eye patch with a mock eye set in blue sapphire and moonstone (2500g)
* Fine gold chain set with a fire opal (2500g)
* Gold circlet set with four aquamarines (2500g)
* Gold music box, ×2 (2500g)
* Old masterpiece painting, ×2 (2500g)
* Platinum bracelet set with a sapphire, ×2 (2500g)

* Holomap of Quel'thalas with Major Image
### Library 
1200g worth of books on city history, dynasties, philosophy, nature, elven translations of histories, creatures and monsters, general knowledge, professions, star maps, maps of kingdoms, etc. 

* 3 Empty Books
* 9 General Books & Poetry
* Many Human Culture/History books
* Several Navigation/Boat Help books
* Good water logs
* Smuggler/Criminal Map
* Weathered Navigation Tools
* Demonology Book - Henry Gallagher's Guide Part 7 - (Yugoloths)
* Demonology Book - Henry Gallagher's Guide Part 2
* Demonology Book - Henry Gallagher's Guide Part 1 - Complete Demon Categorization Vol 1 & II (Devils & Demons)
* Book on Volcanos
* A History of Tropical Diseases
* Lord of the Donuts
* History of Luskan (elven edition)
* Book on Orcish Traditions

### Deck
* 2 cannons (600ft/.5 mile) -> Symbol of Torm: Cannonballs shot by these are considered magical. +1 to hit.
* 21/30 cannonballs
### Hull of Ship
* Beautiful Ice Piercer Signature
### Mess Hall
* Hallow Mark in cabinet behind goods
* Purple Worm Poison Stinger hung up
### Galley
* Dream Kitchen - saucepans, oven, chef's knives, etc.
### Sick Bay
### Crow's Nest
 * Has a telescope
### Officer Quarters
#### Simon's Room
#### Lilith's Room
#### Ibryen's Room
#### Leo's Old Room
#### Alanar's Room
#### Kael's New Room
 * Portrait of his sister hanging up
### Immanuel's Room
 * Shrine to Torm - (Word of Recall). Has 9 heavy weapons and Atticus' scimitar.
#### Hottub 
 * Magical Hot tub, big enough to fit an orc.
 * Keyword "Rinse Spittle"
 * Pours water to fill the tub at the desired temperature. Water disappears when it leaves the tub.
### Guest Cabin
### Jail Cell
### Workshop
### Dry Storage Area
### Treebeard's Garden (inlaid)
* Electrum Throne on Garden
* Glass Statue of Kael'thas Sunstrider (impeccable)
### Main Cargo Hold (100 tons)
* 108 x days of rations
* 60 barrels of water
* Woodcarver's Tools
* Carpenter's Tools
* Paint
* Enough x Wood (until asked 0/2)
* 10 Boulders
* Knickknacks, gods, interior decorations (5g)
* Fine Parchment, Ink, Journals
* 2g Cooking Ingredients
* Barrel of Oil
* Barrel of Tar
* 1 x Potion of Speed
* 250g x 2 gold bracelets, tapestry, silver necklace
* 0 x Ring of Swimming
* 0 x Mariner's Armor (Plate)
* 0 x Vicious Longsword
* 0 x Vancian Helm (Lilith, discord)
* 0 x Ogremoch's Fingertip (Ibrian, no attunement, 10 lb, once per dawn, "arise" to conjure elemental like elemental gem)
* 0 x Stone of Good Luck (Immanuel, requires attunement, +1 saves/ability checks)
* 1 x Sharindlar's Burning Needle (attune, Aura of Purity, Aura of Vitality, 1/day, dawn, revivify 1x ever) -> discord

# Navigational / Important Events Log
* March 21st - Departed Luskan
* March 28 - Arrived at Straits of Kadir
* March 30th - Arrived at Fair Isle Port
* March 31st - Departed Fair Isle Port
* March 31st - Captured the Sea Hawk
* April 6th - Arrived outside Seven Lake Harbor
* April 9th - Left Seven Lake Harbor (due to being attacked by assassins)
* April 12th - Immeral Yaeldrin arrives
* April 15th - Arrived at White Wharf Harbor
* April 17th - Left White Wharf Harbor
* April 19th - Captured The Kingfisher
* April 25th - Immeral Yaeldrin leaves for Ipakas
* April 28th - Arrived at Mount Marsarabit
* May 3rd - Left for Ipakas on resupply mission
* May 7th - Arrived at Ipakas
* May 7th - Left Ipakas
* May 10th - Arrived at Mount Marsarabit