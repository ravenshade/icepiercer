<!-- TITLE: The Flappy Flamingo -->
<!-- SUBTITLE: A quick summary of The Flappy Flamingo -->

# Essentials / TLDR
## Systems

Hull: 425/425 hitpoints (damage threshold 25), AC 16
Control -> Helm: Move up to speed of water wheels, with one 90 degree turn
Movement -> Wheels: 200/200 hitpoints (damage threshhold 25), AC 18. Speed: 45 (regardless of wind)
Power system -> explained later
Sensory system -> Blindsight out to 240 feet. 3d model of the ship and surrounding area displayed in a crystal ball.
Weapon system -> Trebuchet: 150/150 https://5e.tools/objects.html#trebuchet_dmg - 33 stones remaining


## Crew

The crew is composed of constructs, all programmed to obey. As constructs, they do not require air, food, drink, or sleep. They make all checks germaine to handling a water vehicle with a +5 bonus to their check.

### Captain
(1) Nimblewright, as per https://5e.tools/bestiary.html#nimblewright_wdh with the following changes: 85 maximum health.

### Plebian crew
(8) Animated Armors, as per https://5e.tools/bestiary.html#animated%20armor_mm

### Living weapon systems
(1) Oaken Bolter, as per https://5e.tools/bestiary.html#oaken%20bolter_mtf

### Utility / Defense
(1) Rug of Smothering https://5e.tools/bestiary.html#rug%20of%20smothering_mm
(1) Cooking robot - makes Cook's Utensils checks with a +7


